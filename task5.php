<!DOCTYPE html>
<html>
<head>
    <title>Personalized Greeting</title>
</head>
<body>
    <?php
    // Check if the form has been submitted
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Retrieve the user's name from the form
        $userName = $_POST["name"];

        // Check if the name is not empty
        if (!empty($userName)) {
            // Output the personalized greeting message
            echo "Hello, $userName! Welcome to our website.";
        } else {
            // If the name is empty, display an error message
            echo "Please enter your name.";
        }
    }
    ?>

    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="name">Enter your name:</label>
        <input type="text" name="name" id="name" required>
        <input type="submit" value="Submit">
    </form>
</body>
</html>
